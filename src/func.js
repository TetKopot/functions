const getSum = (str1, str2) => {
  let a = str1,
      b = str2;
  if(typeof str1 === 'string' && typeof str2 === 'string'){
    if(str1.length === 0){ 
      a = '0';
    }
    if(str2.length === 0){
      b = '0';
    }
    if(/^\d*$/.test(a) &&  /^\d*$/.test(b)){
      return (+a + +b + '') ;
    }
    else{
      return false;
    }
  }
  else{
    return false;
  }


};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0,
      comments = 0;
  for(let i of listOfPosts){
    if(i.author === authorName){
      posts++;
    }
  }
  for(let j in listOfPosts){
    for(let m in listOfPosts[j].comments){
      if(listOfPosts[j].comments[m].author === authorName){
        comments++;
      }
    }
  }
  return `Post:${posts},comments:${comments}`;

};

const tickets=(people)=> {
  var a25 = 0,a50 = 0;
  for(var i = 0;i<people.length;i++){
    if(people[i] == 25){
      a25 += 1;
    }
    if(people[i] == 50){
      a25 -= 1; a50 += 1;
    }
    if(people[i] == 100){
      if(a50 == 0 && a25 >= 3){
        a25 -= 3;
      }else{
        a25 -= 1; a50 -= 1;
      }
    }
    if(a25 < 0 || a50 < 0){
       return 'NO';
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
